#ifndef OAUTH2_H
#define OAUTH2_H

#include <QObject>
#include <QOAuth2AuthorizationCodeFlow>
#include <QNetworkReply>

class OAuth2 : public QObject
{
    Q_OBJECT
public:
    OAuth2(QObject *parent = nullptr);

    QOAuth2AuthorizationCodeFlow *getGoogle() const;
    void getGoogleCalendarAuthorization();
    void getGoogleDriveAuthorization();
    void setInformation();
    ~OAuth2();
    void refreshAccessTokenAtBeginning(QString fileName);
    bool loggedIn(QString fileName);
    void refreshAccessToken();

private:
    QOAuth2AuthorizationCodeFlow * m_google;
    void authorize(QString scope);  
};

#endif // OAUTH2_H
