#ifndef EVENTINFO_H
#define EVENTINFO_H

#include <QMainWindow>
#include <QListWidgetItem>
#include "event.h"

namespace Ui {
class EventInfo;
}

class EventInfo : public QMainWindow
{
    Q_OBJECT

public:
    explicit EventInfo(QWidget *parent = nullptr);
    ~EventInfo();
    void setEventData(QString id);

private slots:
    void on_pbBack_clicked();
    void on_pbUpdate_clicked();
    void on_pbDelete_clicked();
    void on_listWidget_itemClicked(QListWidgetItem *item);

signals:
    void eventDeleted(QString id);

private:
    Ui::EventInfo *ui;
};

#endif // EVENTINFO_H
