﻿#ifndef CALENDARMANAGER_H
#define CALENDARMANAGER_H

#include <QObject>
#include "oauth2.h"
#include <QUrl>
#include <QString>
#include <QVector>
#include "event.h"

class CalendarManager: public QObject
{
    Q_OBJECT
public:
    CalendarManager(QObject *parent, OAuth2* oauth2);

    void getCalendarIds();
    void getEventsFromCalendar(QString calendarId);

    void createEvent(Event* event);
    void updateEvent(Event* event);
    void deleteEvent(QString eventId);

    QVector<QString> getIds() const;
    QVector<Event*> getEventsFromCalendar() const;

    void setCalendarId(const QString &calendarId);
    QString getCalendarId() const;

    void grantAccess();


    OAuth2 *getAuth() const;


private slots:
    void onSavedCalendarIdsToJsonObject();
    void onSavedCalendarEventsToJsonObject();
    void onTokenChanged();

signals:
    void savedCalendarIdsToJsonObject();
    void savedCalendarEventsToJsonObject();
    void savedCalendarIds();
    void savedEventsFromCalendar();

private:

    OAuth2* m_auth;
    QVector<QString> m_ids;
    QVector<Event*> m_eventsFromCalendar;
    QJsonObject* m_jsonObject;
    QString m_calendarId;

    const QString fromEventToString(Event* event);
    const QUrl getUrlEvents(QString calendarId);
    const QUrl getUrlEvent(QString calendarId, QString eventId);

};

#endif // CALENDARMANAGER_H
