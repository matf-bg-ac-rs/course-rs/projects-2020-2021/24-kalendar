﻿#ifndef MAINSCREEN_H
#define MAINSCREEN_H

#include <QWidget>
#include "loginwindow.h"
#include "calendarwindow.h"
#include "calendarmanager.h"
#include "eventwindow.h"
#include "event.h"
#include "eventinfo.h"

namespace Ui {
class MainScreen;
}

class MainScreen : public QWidget
{
    Q_OBJECT

public:
    explicit MainScreen(QWidget *parent = 0, CalendarManager* calendarManager=nullptr);
    ~MainScreen();

    CalendarManager *calendarManager() const;

    void setLoginWindow(LoginWindow *loginWindow);

    EventWindow *eventWindow() const;

    CalendarWindow *calendarWindow() const;

    EventInfo *eventInfo() const;

private slots:
    void on_pbCalendar_clicked();
    void on_pbEvent_clicked();

    void onSavedCalendarIds();
    void onSavedEventsFromCalendar();
    void onEventSaved(Event *event);
    void onEventUpdated(Event* event);
    void onEventDeleted(QString id);

    void on_pbQuit_clicked();

private:
    Ui::MainScreen *ui;
    CalendarManager* m_calendarManager;
    LoginWindow* m_loginWindow;
    CalendarWindow* m_calendarWindow;
    EventWindow* m_eventWindow;
    EventInfo* m_eventInfo;

};

#endif // MAINSCREEN_H
