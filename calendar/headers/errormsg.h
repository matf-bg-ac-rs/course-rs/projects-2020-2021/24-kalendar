#ifndef ERRORMSG_H
#define ERRORMSG_H

#include <QString>
#include <QMessageBox>
#include <QMainWindow>


class ErrorMsg
{
public:
    ErrorMsg(QWidget* parent);

    void writeMsg(QString msg);
    void moveBox();

private:
    QMessageBox msgBox;
};

#endif // ERRORMSG_H
