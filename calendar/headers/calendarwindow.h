#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidgetItem>
namespace Ui {
class MainWindow;
}

class CalendarWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit CalendarWindow(QWidget *parent = 0);
    ~CalendarWindow();
    void resetWindow();
    void setCalendarWidgetProperties();

private slots:
    void on_pbBack_clicked();
    void on_calendarWidget_activated(const QDate &date);
    void on_calendarWidget_clicked(const QDate &date);
    void on_tableWidget_itemClicked(QTableWidgetItem *item);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
