#ifndef EVENT_H
#define EVENT_H

#include <QObject>
#include <QString>
#include "eventwindow.h"
#include "attachmentinfo.h"

enum Status{
  canceled,
  confirmed,
  tentative
};
Status stringToStatus(QString status);
class Event
{
public:

    Event();
    Event(QString id,QString title,QString description,QDate startDate,QDate endDate,QTime startTime,QTime endTime);
    Event(QString title,QString description,QDate startDate,QDate endDate,QTime startTime,QTime endTime);
    Event(Event *event, QDate startDate, QDate endDate);

    void set_title(Ui::EventWindow *ui);
    void set_description(Ui::EventWindow *ui);
    void set_startDate(Ui::EventWindow *ui);
    void set_endDate(Ui::EventWindow *ui);
    void set_startTime(Ui::EventWindow *ui);
    void set_endTime(Ui::EventWindow *ui);
    void set_repeat(Ui::EventWindow *ui);
    void set_duration(Ui::EventWindow *ui);
    void set_interval(Ui::EventWindow *ui);

    void set_alert(Ui::EventWindow *ui);
    QString alertToString();

    QVector<AttachmentInfo*> getAttachments() const;
    void setAttachments(const QVector<AttachmentInfo*> &value);
    void addAttachment(AttachmentInfo* attachment);

    enum Repeat {
        Dont_repeat,
        Days,
        Weeks,
        Months,
        Years
    };

    Repeat m_repeat = Dont_repeat;
    bool lastsAllDay = false;

    QString get_title();
    QString get_description();
    QDate get_startDate();
    QDate get_endDate();
    QTime get_startTime();
    QTime get_endTime();
    Repeat get_repeat();
    QString recurrenceToString();
    unsigned convertToMinutes();

    QString getId() const;
    void setId(const QString &id);
    QString getAlertType() const;
    void setAlertType(const QString &alertType);
    unsigned getAlertNum() const;
    void setAlertNum(const unsigned &alertNum);
    bool getNoAlert() const;
    void setNoAlert(bool noAlert);
    bool getUseDefaultAlert() const;
    void setUseDefaultAlert(bool useDefaultAlert);
    int getDuration() const;
    void setDuration(int duration);
    int getInterval() const;
    void setInterval(int interval);

protected:

    QString m_id = nullptr;
    QString m_title;
    QString m_description;
    QDate m_startDate;
    QDate m_endDate;
    QTime m_startTime;
    QTime m_endTime;

    QString m_alertType = "popup";
    unsigned m_alertNum = 0;

    bool m_noAlert = false;
    bool m_useDefaultAlert = false;

    int m_duration = 1;
    int m_interval = 1;

private:
    QVector<AttachmentInfo*> m_attachments;
};

#endif // EVENT_H
