#ifndef EVENTWINDOW_H
#define EVENTWINDOW_H

#include <QMainWindow>
#include <QDate>
#include <QFile>
#include "attachmentmanager.h"

class Event;

namespace Ui {
class EventWindow;
}

class EventWindow : public QMainWindow
{
    Q_OBJECT

public:
     EventWindow(QWidget *parent = 0);
     void setEvent(QString title,
                   QDate startDate, QDate endDate,
                   QTime startTime, QTime endTime,
                   QString description,
                   QString alertType, unsigned alertNum,
                   QString repeat, int duration,int interval,
                   QVector<AttachmentInfo*> attachments);


    ~EventWindow();
    void resetWindow();
    void setStartAndEndDate(QDate& date);
    void setOldEventId(const QString &oldEventId);

signals:
    void eventSaved(Event *e);
    void eventUpdated(Event *e);
public slots:

    void showAttachmentWidgets();

private slots:

    void on_pbCancel_clicked();
    void on_pbSave_clicked();      
    void on_pbBrowseAttachment_clicked();
    void on_pbUpload_clicked();
    void on_pbLogin_clicked();
    void on_pbRemoveAttachment_clicked();
    void on_rbNoAlert_clicked();
    void on_rbAllDay_clicked();
    void on_rBRepeat_clicked();

    void onFileUploaded();
private:
    Ui::EventWindow *ui;

    AttachmentManager* m_attachmentManager = nullptr;
    QVector<QFile*> m_files;
    bool m_updateEvent = false;
    QString m_oldEventId;


    void sendFileToGoogleDrive();

};

#endif // EVENTWINDOW_H
