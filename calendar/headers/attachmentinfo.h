#ifndef ATTACHMENTINFO_H
#define ATTACHMENTINFO_H

#include <QString>

class AttachmentInfo
{
public:
    AttachmentInfo(QString fileId, QString fileUrl, QString mimeType, QString name, QString iconLink);

    QString fileId() const;

    QString fileUrl() const;

    QString mimeType() const;

    QString name() const;

    QString iconLink() const;

private:
    QString m_fileId;
    QString m_fileUrl;
    QString m_mimeType;
    QString m_name;
    QString m_iconLink;
};

#endif // ATTACHMENTINFO_H
