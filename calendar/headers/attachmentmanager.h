#ifndef ATTACHMENT_H
#define ATTACHMENT_H

#include <QFile>
#include "oauth2.h"

class AttachmentInfo;
class AttachmentManager : public QObject
{
    Q_OBJECT
public:
    AttachmentManager();
    void uploadAttachment(QFile* file);
    void grantAccess();

    QVector<AttachmentInfo*> attachmnets;


    OAuth2 *auth() const;
    void setAuth(OAuth2 *auth);

signals:
    void accessGrantedSignal();
    void fileUploaded();

private slots:
    void onGranted();
    void onFinished();
    void onTokenChanged();

private:

    OAuth2* m_auth;
    QNetworkReply* m_reply;    

};

#endif // ATTACHMENT_H
