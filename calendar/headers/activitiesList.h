#ifndef ACTIVITIESLIST_H
#define ACTIVITIESLIST_H

#include "event.h"
#include <QList>
#include <QRandomGenerator>


class ActivitiesList
{
public:
    ActivitiesList();
    static ActivitiesList& getInstance()
    {
        static ActivitiesList instance;
        return instance;
    }

    QList<Event*>* getEventsList() { return &eventsList; }
    QList<Event*>* getWeekEvents(const QDate& date);
    void new_event(Event *e);
    void repeatEvent(Event* event);
private:

     QList<Event*> eventsList;
     quint64 gen_random();
};

#endif // ActivitiesList_H
