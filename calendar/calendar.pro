#-------------------------------------------------
#
# Project created by QtCreator 2020-11-20T22:54:09
#
#-------------------------------------------------

QT       += charts core gui network networkauth

QMAKE_CXXFLAGS += --coverage
QMAKE_LFLAGS += --coverage

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = calendar
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    sources/loginwindow.cpp \
    sources/errormsg.cpp \
    sources/eventinfo.cpp \
    sources/attachmentinfo.cpp \
    sources/attachmentmanager.cpp \
    sources/activitiesList.cpp \
    sources/calendarmanager.cpp \
    sources/event.cpp \
    sources/eventwindow.cpp \
    sources/main.cpp \
    sources/mainscreen.cpp \
    sources/calendarwindow.cpp \
    sources/oauth2.cpp

HEADERS += \
    headers/loginwindow.h \
    headers/eventinfo.h \
    headers/attachmentinfo.h \
    headers/attachmentmanager.h \
    headers/activitiesList.h \
    headers/calendarmanager.h \
    headers/event.h \
    headers/eventwindow.h \
    headers/mainscreen.h \
    headers/calendarwindow.h \
    headers/oauth2.h \
    headers/errormsg.h

FORMS += \
    forms/eventinfo.ui \
    forms/eventwindow.ui \
    forms/loginwindow.ui \
    forms/mainscreen.ui \
    forms/calendarwindow.ui

RESOURCES += \
    resources/images.qrc

DISTFILES += \
    resources/calendar_icon.png \
    resources/credentials.json \
    resources/user.png

