# Project 24-Kalendar :calendar:

- This application is a desktop version of Google calendar.
- You can log in to your google account, and the information from your google calendar will be synchronized with our app.
- You have the ability to add, delete and change events, with that kind of power you can control your own destiny.
- Even adding attachments is an option that you should take full advantage of. You can become a master in planning.
- This is an application of the future, for your future.
- Order now and get a bonus riddle to intrigue your mind, you beautiful human being.

## Qt Installation :wrench:

- click on this link: https://www.qt.io/download-open-source?hsCtaTracking=9f6a2170-a938-42df-a8e2-a9f0b1d6cdce%7C6cb0de4f-9bb5-4778-ab02-bfb62735f3e5
- click "download the qt online installer"
- go to the directory where you downloaded the installer
- run  ./qt-unified-linux-x64-4.0.0-online.run
- if you can't, you have to add execute option, you can do that by typing chmod +x qt-unified-linux-x64-4.0.0-online.run
- follow the instructions
- check the "Qt Network Authorization" and "OpenSSL Toolkit" boxes
- finnish installation

## How to run the application :arrow_forward:

- Install the Qt library and Qt Creator and clone the repository
- In 24-kalendar folder you can find file calendar.pro
- Open calendar.pro with Qt Creator
- To build and run the program you need to click the green run button

## Developers :computer:

- [Katarina Simic, 391/2017](https://gitlab.com/SimicKatarina)
- [Milica Sudar, 79/2017](https://gitlab.com/Milica-Sudar)
- [Matija Ilic, 477/2018](https://gitlab.com/IlicMatija)
- [Emilija Stosic, 154/2017](https://gitlab.com/Emilija100sic)


![Mainscreen](calendar/screenshots/mainscreen.jpeg)
![Login](calendar/screenshots/login.jpeg)
![Event](calendar/screenshots/event.jpeg)
![Calendar](calendar/screenshots/calendar.jpeg)
![Attachment](calendar/screenshots/attachment.jpeg)


